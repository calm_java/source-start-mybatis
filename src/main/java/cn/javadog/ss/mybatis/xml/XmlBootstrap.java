package cn.javadog.ss.mybatis.xml;

import java.io.IOException;
import java.io.InputStream;

import cn.javadog.ss.mybatis.common.Tl;
import cn.javadog.ss.mybatis.common.TlMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * @author 余勇
 * @date 2019年11月28日 11:20:00
 */
public class XmlBootstrap {

	public static void main(String[] args) throws IOException {
		String resource = "mybatis-config.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(inputStream);

		TlMapper tlMapper = factory.openSession().getMapper(TlMapper.class);
		Tl tl = tlMapper.findOneResult();
		System.out.println(tl);
	}

}
