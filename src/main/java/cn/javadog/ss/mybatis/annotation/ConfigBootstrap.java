package cn.javadog.ss.mybatis.annotation;

import javax.sql.DataSource;

import cn.javadog.ss.mybatis.common.Tl;
import cn.javadog.ss.mybatis.common.TlMapper;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

/**
 * @author 余勇
 * @date 2019年11月27日 22:14:00
 * //todo 通过代码配置无法关联到mapper.xml
 */
public class ConfigBootstrap {
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String URL = "jdbc:mysql://localhost:3306/mysql_learn?useSSL=true";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "root";

	public static void main(String[] args) {
		SqlSessionFactory factory = getSqlSessionFactory();
		TlMapper tlMapper = factory.openSession().getMapper(TlMapper.class);
		Tl tl = tlMapper.findOneResult();
		System.out.println(tl);
	}

	public static SqlSessionFactory getSqlSessionFactory() {
		DataSource dataSource = new PooledDataSource(DRIVER, URL, USERNAME, PASSWORD);
		TransactionFactory transactionFactory = new JdbcTransactionFactory();
		Environment environment = new Environment("development", transactionFactory, dataSource);
		Configuration configuration = new Configuration(environment);
		configuration.addMapper(TlMapper.class);
		return new SqlSessionFactoryBuilder().build(configuration);
	}

}
