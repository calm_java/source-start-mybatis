package cn.javadog.ss.mybatis.common;

import lombok.Data;

/**
 * @author 余勇
 * @date 2019年11月28日 10:33:00
 */
@Data
public class Tl {

	private int id;
	private String city;

}
