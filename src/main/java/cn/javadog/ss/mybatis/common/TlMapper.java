package cn.javadog.ss.mybatis.common;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author 余勇
 * @date 2019年11月27日 22:21:00
 */
@Mapper
public interface TlMapper {

	// @Select("SELECT * FROM t_like LIMIT 1")
	Tl findOneResult();

}
